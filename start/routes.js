'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

const Route = use('Route')

Route.on('/').render('welcome')

// Auth Group
Route.group(() => {
  Route.post('/login', 'User/UserController.login')
  Route.post('/register', 'User/UserController.register')
}).prefix('api/v1')

// User Group
Route.group(() => {
  Route.get('/user', 'User/UserController.show')
}).prefix('api/v1').middleware('auth')

// Todos Group
Route.group(() => {
  Route.get('/todo/get', 'Todo/TodoController.show')
  Route.get('/todo/get/total', 'Todo/TodoController.countTotal')
  Route.post('/todo/create', 'Todo/TodoController.store')
  Route.patch('/todo/update/:id', 'Todo/TodoController.update')
  Route.delete('/todo/delete/:id', 'Todo/TodoController.delete')
  Route.delete('/todo/deleteAll', 'Todo/TodoController.deleteAll')
}).prefix('api/v1').middleware('auth')


