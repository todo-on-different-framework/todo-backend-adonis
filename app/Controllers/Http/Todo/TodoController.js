'use strict'

const Todo = use('App/Models/Todo/Todo')

class TodoController {

  async show({ response, auth }) {

    const user = auth.current.user

    const query = Todo.query()
      .where('user_id', user.id)

    response.status(200).send({ data: await query.fetch() })
  }

  async countTotal({ response, auth }) {

    const user = auth.current.user

    const query = Todo.query()
      .where('user_id', user.id)

    response.status(200).send({ data: await query.count('* as totalTodos') })
  }

  async store({ request, response, auth }) {

    const user_id = auth.current.user.id

    const allParams = request.only(['todo_text', 'completed'])

    const createTodo = await Todo.create({
      user_id: user_id,
      todo_text: allParams.todo_text,
      completed: allParams.completed
    })

    response.status(200).send({ data: createTodo })
  }

  async update({ request, response, auth, params }) {

    const user = auth.current.user

    const allParams = request.only(['todo_text', 'completed'])

   const updateTodo =  await Todo.query()
      .where('id', params.id)
      .where('user_id', user.id)
      .update({todo_text: allParams.todo_text, completed: allParams.completed})

    if (updateTodo === 0) return response.status(401).send({message: 'You are unauthorized to update this todo!'})

    const getUpdatedTodo = Todo.query()
      .where('id', params.id)

    response.status(200).send({ data: await getUpdatedTodo.fetch(), message: 'Todo has been added successfully!' })
  }

  async delete({ request, response, auth, params }) {

    const user = auth.current.user

    const deleteTodo = await Todo.query()
      .where('id', params.id)
      .where('user_id', user.id)
      .delete()

    if (deleteTodo === 0) return response.status(401).send({message: 'You are unauthorized to delete this todo!'})

    response.status(200).send({ message: 'Todo has been deleted successfully!' })
  }

  async deleteAll({ response, auth }) {

    const user = auth.current.user

    const deleteTodo = await Todo.query()
      .where('user_id', user.id)
      .delete()

    if (deleteTodo === 0) return response.status(401).send({message: 'You are unauthorized to delete todos!'})

    response.status(200).send({ message: 'Todos has been deleted successfully!' })
  }

}

module.exports = TodoController
