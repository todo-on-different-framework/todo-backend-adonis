'use strict'

const User = use('App/Models/User')

class UserController {

  async login ({ request, response, auth }) {
    const allParams = request.only(['email', 'password'])
    await auth.attempt(allParams.email, allParams.password)

    const user = await User.findBy('email', allParams.email)

    const token = await auth.withRefreshToken().generate(user)

    response.ok({user, token: token.token, refreshToken: token.refreshToken,  message: 'Logged in successfully'})

  }

  // async refreshToken({ request, response, auth }) {
  //   const refreshToken = request.input('refresh_token')
  //
  //   if (!refreshToken) return response.badRequest('error.invalidToken')
  //
  //   // generate new token from refresh token
  //   const tokenData = await auth.newRefreshToken().generateForRefreshToken(refreshToken)
  //   response.ok({
  //     token: tokenData.token,
  //     refreshToken: tokenData.refreshToken
  //   })
  // }

  async register ({ request, response }) {

    const allParams = request.only(['username', 'email', 'password'])

    const checkIfEmailExist = await User.findBy('email', allParams.email)
    const checkIfUsernameExist = await User.findBy('username', allParams.username)

    if(checkIfEmailExist) {
      return response.status(409).send({message: 'Email is already used!'})
    } else if(checkIfUsernameExist){
      return response.status(409).send({message: 'Username is already taken!'})
    }

    const user = await User.create({
      username: allParams.username,
      email: allParams.email,
      password: allParams.password
    })

    return response.ok(user)

  }

  async show({ request, response, auth }) {
    const user = auth.current.user

    const query = User.query()
      .where('email', user.email)

    response.status(200).send({ data: await query.fetch() })
  }

}

module.exports = UserController
